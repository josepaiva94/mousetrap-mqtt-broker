# MQTT Broker for MouseTrap

This project contains a Node.js MQTT Broker that enables lightweight communication
for MouseTrap. The Broker is secured with Auth0. Only authenticated devices can
publish or subscribe topics, and just the topics under `user_metadata.topics` in
its device profile on Auth0.

The communication is encrypted via node's TLS implementation. This provides secure
communication between broker and client.

## Getting Started

Clone the repo or download it.

## Requirements

Make sure that you have redis running at `localhost:6379`

## Setup the `.env` File

You need to create an `.env` file in the project root with the following content.

```
AUTH0_CLIENT_ID=nxXq9GaUdUnaWEF0YhwBoaAmBy5ABRmj
AUTH0_DOMAIN=mousetrap.eu.auth0.com
AUTH0_CONNECTION=Traps

```

## Setup certificate files

You need to go into `certs` directory and run the following commands

```bash
openssl genrsa -out mqtt-broker-key.pem 2048
openssl req -new -sha256 -key mqtt-broker-key.pem -out mqtt-broker-csr.pem
openssl x509 -req -in mqtt-broker-csr.pem -signkey mqtt-broker-key.pem -out mqtt-broker-cert.pem

```

## Install the Dependencies and Start the API

```bash
npm install
npm start
```

The Broker will be served at `http://localhost:8443`.

## How to use

Please check `example` directory.

## Issue Reporting

If you have found a bug or if you have a feature request, please report them at this repository issues section. Please do not report security vulnerabilities on the public issues section, instead send an email
to up201200272 [at] fc.up.pt

## Author

[José C. Paiva](#)

## License

This project is licensed under the MIT license. See the [LICENSE](LICENSE.txt) file for more info.
