/**
 * MQTT broker which requires authentication delegated on Auth0
 *
 * @author José C. Paiva <up201200272@fc.up.pt>
 */

const path = require('path');
const jwksRsa = require('jwks-rsa');
const mosca = require('mosca');
require('dotenv').config();

const Auth0Mosca = require('./auth0mosca');

const SECURE_KEY = path.join(__dirname, 'certs/mqtt-broker-key.pem');
const SECURE_CERT = path.join(__dirname, 'certs/mqtt-broker-cert.pem');

const ascoltatore = {
    //using ascoltatore
    type: 'redis',
    redis: require('redis'),
    db: 12,
    port: process.env.REDIS_PORT || 6379,
    return_buffers: true, // to handle binary payloads
    host: process.env.REDIS_HOST || "localhost"
};

const settings = {
    port: 8443,
    backend: ascoltatore,
    persistence: {
        factory: mosca.persistence.Redis,
        port: process.env.REDIS_PORT || 6379,
        host: process.env.REDIS_HOST || "localhost"
    },
    // commented out to simplify clients
    // secure: {
    //     port: 8443,
    //     keyPath: SECURE_KEY,
    //     certPath: SECURE_CERT,
    // }
};

if (!process.env.AUTH0_DOMAIN || !process.env.AUTH0_CLIENT_ID ||
    !process.env.AUTH0_CONNECTION) {
    throw 'Make sure you have AUTH0_DOMAIN, AUTH0_CLIENT_ID' +
        ' and AUTH0_CONNECTION in your .env file';
}

// options for JWT verification
const jwtOptions = {
    // Validate the audience and the issuer.
    audience: process.env.AUTH0_CLIENT_ID,
    issuer: `https://${process.env.AUTH0_DOMAIN}/`,
    algorithms: ['RS256']
};

const jwtClient = jwksRsa({
    strictSsl: true,
    jwksUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`
});

jwtClient.getSigningKeys(function(err, keys) {

    var publicKey = keys[0].publicKey || keys[0].rsaPublicKey;

    startServer(publicKey);
});

function startServer(publicKey) {

    var auth0 = new Auth0Mosca(process.env.AUTH0_DOMAIN,
        process.env.AUTH0_CLIENT_ID, process.env.AUTH0_CONNECTION, jwtOptions,
        publicKey);

    const server = new mosca.Server(settings);

    // delegate auth tasks of Mosca on Auth0Mosca
    server.authenticate = auth0.authenticateWithJWT();
    server.authorizePublish = auth0.authorizePublish();
    server.authorizeSubscribe = auth0.authorizeSubscribe();

    // fired when a client establishes a connection
    server.on('clientConnected', function(client) {
        console.log('New connection: ', client.id);
    });

    // fired when a message is received
    server.on('published', function(packet, client) {

        console.log('Published on:', packet.topic, '| payload:', packet.payload.toString('utf8'));
    });

    server.on('ready', setup);
}

// MQTT server is ready
function setup() {
    console.log('Mosca server is up and running');
}
