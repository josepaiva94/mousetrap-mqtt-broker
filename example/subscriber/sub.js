/**
 * Example of a MQTT subscriber
 *
 * @author José C. Paiva <up201200272@fc.up.pt>
 */

const mqtt = require('mqtt');

// settings for the connection
const settings = {
    port: 8443,
    rejectUnauthorized: false,
    clientId: 'sub-' + Date.now(),
    username: 'JWT',
    password: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJFTkROa1l6TkRJNE1qa3pNemM0UmpBeE16WXhORGMwTURreFEwRkZOVUpEUVRBMlFqUXhRdyJ9.eyJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImVtYWlsIjoidXAyMDEyMDAyNzNAZmMudXAucHQiLCJjbGllbnRJRCI6Im54WHE5R2FVZFVuYVdFRjBZaHdCb2FBbUJ5NUFCUm1qIiwidXBkYXRlZF9hdCI6IjIwMTctMDMtMjlUMDg6NDE6MTYuOTY5WiIsIm5hbWUiOiJ1cDIwMTIwMDI3M0BmYy51cC5wdCIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci85YTcyNjdjNmFkNmJhNTc2NzliMjY5ZDNmYWViMmVhNz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnVwLnBuZyIsInVzZXJfaWQiOiJhdXRoMHw1OGQ3ZjVkYjFhYmRmNDY4N2M2Y2Y3ZjUiLCJuaWNrbmFtZSI6InVwMjAxMjAwMjczIiwiaWRlbnRpdGllcyI6W3sidXNlcl9pZCI6IjU4ZDdmNWRiMWFiZGY0Njg3YzZjZjdmNSIsInByb3ZpZGVyIjoiYXV0aDAiLCJjb25uZWN0aW9uIjoiVHJhcHMiLCJpc1NvY2lhbCI6ZmFsc2V9XSwiY3JlYXRlZF9hdCI6IjIwMTctMDMtMjZUMTc6MDk6NDcuODQ5WiIsInVzZXJfbWV0YWRhdGEiOnsidG9waWNzIjoicHJlc2VuY2UifSwiaXNzIjoiaHR0cHM6Ly9tb3VzZXRyYXAuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDU4ZDdmNWRiMWFiZGY0Njg3YzZjZjdmNSIsImF1ZCI6Im54WHE5R2FVZFVuYVdFRjBZaHdCb2FBbUJ5NUFCUm1qIiwiZXhwIjoxNDkwODEyODc2LCJpYXQiOjE0OTA3NzY4NzZ9.mFxaC0QyU9BRJ340N-QHWWO5uDsjFkmTLaoyUrWKYPtOqde5HLQ-oxpsQdv4SNFB6DaGHCWaoWuGjuHEBpyaCRs5yivrn668Fo9uAt5DfG5Gj40V-gbT0PmtaMCoLDldirgdSinOlEa_vu_Zdf0gnwYxGzLUfzSro-1gqXtMjg7xyPrPrdbpgLiU0zNHp9F7Fr7vA7aQuucsnMaaLe9sfzf_6hDckz_KL8ccW836a0euYuxNdp6VNx6_4DuMlRNpSgsi3-P6Pcdk3EJwaJsu6rY9SMpZqTH6JSiNAKSWXtkZ_zLf7B9OXcvz7ACZ8S_KYSDwMAN0BWc4xrLkuxpwzA'
}

console.log('--');

// client connection
var client = mqtt.connect('mqtts://localhost', settings);

client.on('connect', function() {

    console.log('maybe connected');
    client.subscribe('presence', { qos: 1 });
})

client.on('close', function() {
    console.log('close');
});

client.on('offline', function() {
    console.log('offline');
});

client.on('error', function(e) {
    console.log('error');
    console.log(e);
});

client.on('reconnect', function() {
    console.log('reconnect');
});

client.on('message', function(topic, message, packet) {
    console.log(topic);
    console.log(message);
    console.log(packet);
});
